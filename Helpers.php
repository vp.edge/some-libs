<?php
/**
 * Helpers functions
 * User: EDGE
 * Date: 15.05.2017
 * Time: 15:35
 */

/**
 * Return full URL
 * @param $url
 * @return string
 */
function url($url = '')
{
    return 'http' . (isset($_SERVER['HTTPS']) ? 's' : '') . '://' . $_SERVER['SERVER_NAME'] . '/' . preg_replace('/^\//', '', $url);
}

/**
 * Redirect 2 URL
 * @param $url
 */
function redirect($url)
{
    header('Location: ' . url($url));
}

/**
 * Read GET params
 * @param $name
 * @return mixed|string
 */
function _get($name, $replace = ['\'' => '&#39;', '%' => '\%', '_' => '\_'])
{
    return (isset($_GET[$name]) && $_GET[$name]) ? str_replace(array_keys($replace), $replace, StripSlashes(HtmlSpecialChars(trim($_GET[$name])))) : '';
}

/**
 * Read POST params
 * @param $name
 * @return mixed|string
 */
function _post($name)
{
    return (isset($_POST[$name]) && $_POST[$name]) ? str_replace("'", '&#39;', StripSlashes(HtmlSpecialChars(trim($_POST[$name])))) : '';
}

/**
 * Function 4 debug
 * @param $var
 */
function _debug($var)
{
    echo '<pre>';
    if (is_array($var)) print_r($var);
    else var_dump($var);
    echo '</pre>';
}

/**
 * Pluck from SimpleModel
 * @param $array
 * @param $name
 * @param bool $unique
 * @return array
 */
function _pluck($array, $name, $unique = false)
{
    $result = [];

    foreach($array as $value) {
        if (array_key_exists($name, $value)) {
            array_push($result, $value[$name]);
        }
    }

    return ($unique) ? array_unique($result) : $result;
}

function url_with_get($get)
{
    $get_data = $_GET;

    foreach ($get as $key => $value) $get_data[$key] = $value;
    foreach ($get_data as $name => $value) $get_query[] = "{$name}={$value}";

    return url("{$_SERVER['REDIRECT_URL']}?" . implode('&', $get_query));
}

function random_str($length)
{
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}