<?php
namespace EDGE;

/**
 * Class Mail
 * @package EDGE
 */
class Mail
{
    protected $to;
    protected $subject;
    protected $message;
    protected $headers;
    protected $parameters;

    /**
     * @param string $name
     * @param array $arguments
     */
    function __call($name, $arguments)
    {
        if (method_exists($this, $name)) {
            return call_user_func_array([$this, $name], $arguments);
        } else {
            throw new \Exception("Method '{$name}' does not exist!");
        }
    }

    /**
     * @param string $name
     * @param array $arguments
     */
    public static function __callStatic($name, $arguments)
    {
        if (method_exists(self::class, $name)) {
            return call_user_func_array([(new self), $name], $arguments);
        } else {
            throw new \Exception("Method '{$name}' does not exist!");
        }
    }

    /**
     * @param $email
     * @return $this
     */
    protected function to($email)
    {
        if (is_array($email)) $email = implode(',', $email);
        $this->to = $email;
        return $this;
    }

    /**
     * @param $email
     * @return $this
     */
    protected function from($email)
    {
        $this->headers['from'] = "From: {$email}";
        return $this;
    }

    /**
     * @param $subject
     * @return $this
     */
    protected function subject($subject)
    {
        $this->subject = $subject;
        return $this;
    }

    /**
     * @param $message
     * @param bool $html
     * @param string $charset
     * @return $this
     */
    protected function message($message, $html = true, $charset = "utf-8")
    {
        if ($html) {
            $this->headers['mime'] = "MIME-Version: 1.0";
            $this->headers['content-type'] = "Content-type: text/html; charset={$charset}";
        }
        $this->message = $message;
        return $this;
    }

    /**
     * @return bool
     */
    protected function send()
    {
        $this->headers['x-mailer'] = "X-Mailer: PHP/" . phpversion();
        $mail = mail($this->to, $this->subject, $this->message, implode("\r\n", $this->headers), $this->parameters);
        unset($this);
        return $mail;
    }
}