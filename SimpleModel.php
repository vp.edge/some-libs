<?php
/**
 * Simple model
 * User: EDGE
 * Date: 22.05.2017
 * Time: 12:50
 */
namespace EDGE;

class SimpleModel
{
    protected $table    = '';
    protected $select   = '';
    protected $where    = '';
    protected $orderBy  = '';
    protected $groupBy  = '';
    protected $join     = '';
    protected $limit    = '';
    protected $offset   = '';
    protected $query    = '';
    protected $result   = [];

    /**
     * SimpleModel constructor.
     * @param string|array $table
     */
    function __construct($table = '')
    {
        if (is_array($table)) $table = $this->implode($table, '`');
        else $table = "`{$table}`";

        $this->table = $table;
    }

    /**
     * Очищення об'єкту
     * @return $this
     */
    public function clear($field)
    {
        if ($field == strtolower('*')) {
            $this->select   = '';
            $this->where    = '';
            $this->orderBy  = '';
            $this->groupBy  = '';
            $this->join     = '';
            $this->limit    = '';
            $this->offset   = '';
            $this->query    = '';
            $this->result   = [];
        } else {
            $this->{$field}   = '';
        }

        return $this;
    }

    public function debug()
    {
        echo '<pre>';
        echo "SELECT " . ($this->select ? $this->select : "*") .
            "\r\nFROM {$this->table}" .
            ($this->join ? "\r\n{$this->join}" : null) .
            ($this->where ? "\r\nWHERE {$this->where}" : null) .
            ($this->groupBy ? "\r\nGROUP BY {$this->groupBy}" : null) .
            ($this->orderBy ? "\r\nORDER BY {$this->orderBy}" : null) .
            ($this->limit ? "\r\nLIMIT {$this->limit}" . ($this->offset ? " OFFSET {$this->offset}" : null) : null);
        if (!empty($this->result)) {
            echo "\r\n";
            print_r($this->result);
        }
        echo '</pre>';

        return $this;
    }

    /**
     * Функція для перетворення масиву в текст для запиту
     * @param $array
     * @param string $quote
     * @param string $delim
     * @param bool $type
     * @param bool $dot_repl
     * @return bool|string
     */
    protected function implode($array, $quote = '\'', $delim = ',', $type = false, $dot_repl = true)
    {
        $result = '';

        foreach ($array as $value) {
            if (is_array($value)) {
                $result .= ($value['func'] ? "{$value['func']}(" : null) .
                    ($value['src'] ? $value['src'] : $quote . ($type ? $this->type($value) : $value['column']) . $quote).
                    ($value['func'] ? ")" : null) . ($value['as'] ? " AS {$value['as']}" : null) . $delim;
            } else {
                $result .= $quote . ($type ? $this->type($value) : $value) . $quote . $delim;
            }
        }
        if ($dot_repl) $result = str_replace('.', "{$quote}.{$quote}", $result);

        return (strlen($result)) ? substr($result, 0, -1) : '';
    }

    /**
     * Функція для вставки значень в запит
     * @param $src
     * @return string
     */
    protected function type($src)
    {
        return (is_int($src) ? $src : "'{$src}'");
    }

    /**
     * Вибір таблиці
     * @param $table
     * @return $this
     */
    public function table($table)
    {
        if (is_array($table)) $table = $this->implode($table, '`');
        else $table = "`{$table}`";

        $this->table = $table;

        return $this;
    }

    /**
     * Вибірка з БД
     * @param array $select
     * @return $this
     */
    public function select($select = [], $append = false)
    {
        if (is_string($select)) $this->select = $select;
        else $this->select = ($append) ? $this->select . $this->implode($select, '`') : $this->implode($select, '`');

        return $this;
    }

    /**
     * Умова для запиту. Повторний виклик (І)
     * @param string $opt1 - query (WHERE $opt1)
     * @param string $opt2 - equal query (WHERE $opt1=$opt2)
     * @param string $opt3 - other logic operation query (WHERE $opt1$opt2$opt3)
     * @return SimpleModel $this
     */
    public function where($opt1, $opt2 = '', $opt3 = '')
    {
        if ($this->where) $this->where .= " AND ";
        $opt = str_replace('.', '`.`', $opt1);
        if ($opt3) {
            $this->where .= "`{$opt}`{$opt2}" . $this->type($opt3);
        } elseif ($opt2) {
            $this->where .= "`{$opt}`=" . $this->type($opt2);
        } else {
            $this->where .= $opt1;
        }

        return $this;
    }

    /**
     * Умова для запиту. Повторний виклик (АБО)
     * @param string $opt1 - query (WHERE $opt1)
     * @param string $opt2 - equal query (WHERE $opt1=$opt2)
     * @param string $opt3 - other logic operation query (WHERE $opt1$opt2$opt3)
     * @return SimpleModel $this
     */
    public function orWhere($opt1, $opt2 = '', $opt3 = '')
    {
        if ($this->where) $this->where .= " OR ";
        $opt = str_replace('.', '`.`', $opt1);
        if ($opt3) {
            $this->where .= "`{$opt}`{$opt2}" . $this->type($opt3);
        } elseif ($opt2) {
            $this->where .= "`{$opt}`=" . $this->type($opt2);
        } else {
            $this->where .= $opt1;
        }

        return $this;
    }

    /**
     * Порівняння тексту
     * @param $column
     * @param $like
     * @param $or
     * @return SimpleModel
     */
    public function like($column, $like, $or = false)
    {
        return ($or) ? $this->orWhere($column, ' LIKE ', $like) : $this->where($column, ' LIKE ', $like);
    }

    /**
     * WHERE IN
     * @param $column
     * @param $array
     * @param bool $or
     * @return SimpleModel
     */
    public function whereIn($column, $array, $or = false)
    {
        $opt = "`{$column}` IN ({$this->implode($array, '', ',', true)})";

        return ($or) ? $this->orWhere($opt) : $this->where($opt);
    }

    /**
     * JOIN query
     * @param $type
     * @param $table
     * @param $parent_column
     * @param $column
     * @return $this
     */
    public function joinQuery($type, $table, $on)
    {
        if ($this->join) $this->join .= "\r\n";
        $this->join .= strtoupper($type) . " JOIN {$table} ON {$on}";

        return $this;
    }

    /**
     * JOIN
     * @param $type
     * @param $table
     * @param $parent_column
     * @param $column
     * @return $this
     */
    protected function join($type, $table, $column, $parent_column, $parent_table = null)
    {
        if ($this->join) $this->join .= "\r\n";
        $this->join .= "{$type} JOIN `{$table}` ON " . ($parent_table ? $parent_table : $this->table) . ".`{$parent_column}`=`{$table}`.`{$column}`";

        return $this;
    }

    /**
     * RIGHT JOIN
     * @param $table
     * @param $parent_column
     * @param $column
     * @return SimpleModel
     */
    public function rightJoin($table, $column, $parent_column = 'id', $parent_table = null)
    {
        return $this->join('RIGHT', $table, $column, $parent_column, $parent_table);
    }

    /**
     * LEFT JOIN
     * @param $table
     * @param $parent_column
     * @param $column
     * @return SimpleModel
     */
    public function leftJoin($table, $column, $parent_column = 'id', $parent_table = null)
    {
        return $this->join('LEFT', $table, $column, $parent_column, $parent_table);
    }

    /**
     * FULL OUTER JOIN
     * @param $table
     * @param $parent_column
     * @param $column
     * @return SimpleModel
     */
    public function fullJoin($table, $column, $parent_column = 'id', $parent_table = null)
    {
        return $this->join('FULL OUTER', $table, $column, $parent_column, $parent_table);
    }

    /**
     * INNER JOIN
     * @param $table
     * @param $parent_column
     * @param $column
     * @return SimpleModel
     */
    public function innerJoin($table, $column, $parent_column = 'id', $parent_table = null)
    {
        return $this->join('INNER', $table, $column, $parent_column, $parent_table);
    }

    /**
     * Встановлення сортування для запиту
     * @param string $column
     * @param string $opt
     * @return $this
     */
    public function orderBy($column, $opt = "ASC")
    {
        $column = str_replace('.', '`.`', $column);

        $this->orderBy = (is_array($column) ? $this->implode($column, '`') : "`{$column}`") . " {$opt}";

        return $this;
    }

    /**
     * Встановлення групування для запиту
     * @param string $column
     * @param string $opt
     * @return $this
     */
    public function groupBy($column)
    {
        $column = str_replace('.', '`.`', $column);

        $this->groupBy = (is_array($column)) ? $this->implode($column, '`') : "`{$column}`";

        return $this;
    }

    /**
     * Обмеження для запиту
     * @param $limit
     * @return $this
     */
    public function limit($limit = '')
    {
        $this->limit = $limit;

        return $this;
    }

    /**
     * @param $offset
     * @return $this
     */
    public function offset($offset = '')
    {
        $this->offset = $offset;

        return $this;
    }

    /**
     * Виконання сформованого запиту та запис результату в об'єкт
     * @return $this
     * $this->result => query
     */
    public function make($select = [], $column = null, $clear_result = true)
    {
        if ($clear_result) $this->result = [];

        if (count($select)) $this->select($select);

        if (is_array($select)) {
            $query = mysqlQuery(
                "SELECT " . ($this->select ? $this->select : "*") .
                "\r\nFROM {$this->table}" .
                ($this->join ? "\r\n{$this->join}" : null) .
                ($this->where ? "\r\nWHERE {$this->where}" : null) .
                ($this->groupBy ? "\r\nGROUP BY {$this->groupBy}" : null) .
                ($this->orderBy ? "\r\nORDER BY {$this->orderBy}" : null) .
                ($this->limit ? "\r\nLIMIT {$this->limit}" . ($this->offset ? " OFFSET {$this->offset}" : null) : null)
            );
        } else {
            $query = mysqlQuery($select);
        }


        while($temp = mysqli_fetch_assoc($query))
        {
            if ($temp['id']) {
                $this->result[$temp['id']] = $temp;
            } elseif ($column !== null) {
                $this->result[$temp[$column]] = $temp;
            } else {
                array_push($this->result, $temp);
            }
        }

        return $this;
    }

    /**
     * Кількість записів в БД
     * @return mixed
     */
    public function count($from_result = false, $query = '')
    {
        if ($from_result) return count($this->result);

        $query = mysqlQuery(
            "SELECT COUNT(*)" .
            " FROM {$this->table}" .
            ($query ? " {$query}" : null) .
            ($this->where ? " WHERE {$this->where}" : null)
        );

        return mysqli_fetch_assoc($query)['COUNT(*)'];
    }

    /**
     * Кількість записів в БД
     * @return mixed
     */
    public function extCount($column = '', $query = '')
    {
        if ($column) $column = (is_array($column)) ? $this->implode($column, '`') : "`{$column}`";

        $query = mysqlQuery(
            "SELECT COUNT(" . ($column ? $column : "*") . ")" .
            " FROM {$this->table}" .
            ($query ? " {$query}" : null) .
            ($this->where ? " WHERE {$this->where}" : null)
        );

        return mysqli_fetch_assoc($query)["COUNT(" . ($column ? $column : "*") . ")"];
    }

    /**
     * Отримання результату (масив)
     * @return mixed
     */
    public function toArray()
    {
        return $this->result;
    }

    /**
     * Отримання результату (JSON)
     * @return string
     */
    public function toJSON()
    {
        return json_encode($this->result);
    }

    /**
     * Перший запис з результату
     * @return mixed
     */
    public function first()
    {
        $temp = $this->result;

        return array_shift($temp);
    }

    /**
     * Отримати одну колонку з результату
     * @param $column
     * @return array
     */
    public function pluck($column, $unique = false)
    {
        $result = [];

        foreach($this->result as $value) {
            if (array_key_exists($column, $value)) {
                array_push($result, $value[$column]);
            }
        }

        return ($unique) ? array_unique($result) : $result;
    }

    /**
     * Створення нового запису
     * @param $data
     * @return bool
     */
    public function create($data)
    {
        $keys = '';
        $values = '';

        foreach ($data as $key => $value) {
            $keys .= "`{$key}`,";
            $values .= $this->type($value) . ",";
        }

        $keys = substr($keys, 0, -1);
        $values = substr($values, 0, -1);

        $query = mysqlQuery("INSERT INTO {$this->table}({$keys}) VALUES ({$values})");

        return ($query == 1) ? true : false;
    }

    /**
     * Оновлення запису(ів)
     * @param $data
     * @return bool
     */
    public function update($data)
    {
        $set = '';

        foreach ($data as $key => $value) {
            $set .= "`{$key}`=" . $this->type($value) . ",";
        }

        $set = substr($set, 0, -1);

        $query = mysqlQuery(
            "UPDATE {$this->table} SET {$set}" .
            ($this->where ? " WHERE {$this->where}" : null)
        );

        return ($query == 1) ? true : false;
    }

    /**
     * Видалити з результату
     * @param $id
     * @return $this
     */
    public function delete($id)
    {
        if (isset($this->result[$id])) unset($this->result[$id]);

        return $this;
    }

    /**
     * Видалити з БД
     * @return $this
     */
    public function destroy()
    {
        $query = mysqlQuery(
            "DELETE FROM {$this->table}" .
            ($this->where ? " WHERE {$this->where}" : null)
        );

        return ($query == 1) ? true : false;

        return $this;
    }
}